import { MigrationInterface, QueryRunner } from 'typeorm';

export class linkToProduction1581959158582 implements MigrationInterface {
    name = 'linkToProduction1581959158582';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `CREATE TABLE "production" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "slug" character varying NOT NULL, "logo" character varying NOT NULL, "about" character varying NOT NULL, CONSTRAINT "PK_722753196a878fa7473f0381da3" PRIMARY KEY ("id"))`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TYPE "link_type_enum" AS ENUM('TWITTER', 'VK', 'INSTAGRAM', 'TELEGRAM', 'FACEBOOK', 'YOUTUBE', 'TWITCH', 'DISCORD', 'STEAM', 'DOTABUFF')`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "link" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "url" text NOT NULL, "type" "link_type_enum" NOT NULL, "production_id" uuid, CONSTRAINT "PK_26206fb7186da72fbb9eaa3fac9" PRIMARY KEY ("id"))`,
            undefined,
        );
        await queryRunner.query(
            `ALTER TABLE "link" ADD CONSTRAINT "FK_6c89ff8f3dd385be74c98486174" FOREIGN KEY ("production_id") REFERENCES "production"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
            undefined,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `ALTER TABLE "link" DROP CONSTRAINT "FK_6c89ff8f3dd385be74c98486174"`,
            undefined,
        );
        await queryRunner.query(`DROP TABLE "link"`, undefined);
        await queryRunner.query(`DROP TYPE "link_type_enum"`, undefined);
        await queryRunner.query(`DROP TABLE "production"`, undefined);
    }
}
