import { Column, Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../../common/abstract.entity';
import { LinkEntity } from '../../modules/link/link.entity';
import { SlugTransformer } from '../../shared/slug.transformer';
import { ProductionDto } from './dto/ProductionDto';

@Entity({ name: 'production' })
export class ProductionEntity extends AbstractEntity<ProductionDto> {
    @Column()
    title: string;

    @Column({ transformer: new SlugTransformer() })
    slug: string;

    @Column()
    logo: string;

    @Column()
    about: string;

    @OneToMany(
        () => LinkEntity,
        (link: LinkEntity) => link.production,
    )
    links: LinkEntity[];

    dtoClass = ProductionDto;
}
