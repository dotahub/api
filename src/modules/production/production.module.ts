import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LinkRepository } from '../../modules/link/link.repository';
import { LinkService } from '../../modules/link/link.service';
import { ProductionController } from './production.controller';
import { ProductionRepository } from './production.repository';
import { ProductionService } from './production.service';

@Module({
    imports: [TypeOrmModule.forFeature([ProductionRepository])],
    controllers: [ProductionController],
    providers: [ProductionService, LinkService, LinkRepository],
})
export class ProductionModule {}
