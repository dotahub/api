import { forwardRef, Inject, Injectable } from '@nestjs/common';

import { PageMetaDto } from '../../common/dto/PageMetaDto';
import { FileNotImageException } from '../../exceptions/file-not-image.exception';
import { ProductionNotFoundException } from '../../exceptions/production-not-found.exception';
import { IFile } from '../../interfaces/IFile';
import { LinkService } from '../../modules/link/link.service';
import { AwsS3Service } from '../../shared/services/aws-s3.service';
import { ValidatorService } from '../../shared/services/validator.service';
import { CreateProductionDto } from './dto/CreateProductionDto';
import { ProductionsPageDto } from './dto/ProductionsPageDto';
import { ProductionsPageOptionsDto } from './dto/ProductionsPageOptionsDto';
import { UpdateProductionDto } from './dto/UpdateProductionDto';
import { ProductionEntity } from './production.entity';
import { ProductionRepository } from './production.repository';

@Injectable()
export class ProductionService {
    constructor(
        private readonly _linkService: LinkService,
        public readonly productionRepository: ProductionRepository,
        public readonly validatorService: ValidatorService,
        public readonly awsS3Service: AwsS3Service,
    ) {}

    async addProduction(
        addProductionDto: CreateProductionDto,
        file: IFile,
    ): Promise<ProductionEntity> {
        let logo: string;

        if (file && !this.validatorService.isImage(file.mimetype)) {
            throw new FileNotImageException();
        }

        if (file) {
            logo = await this.awsS3Service.uploadImage(file, 'production');
        }

        const production = this.productionRepository.create({
            ...addProductionDto,
            logo,
        });

        for (const link of addProductionDto.links) {
            const linkEntity = await this._linkService.addLink(link);
            linkEntity.toDto();
            production.links.push(linkEntity);
        }

        return this.productionRepository.save(production);
    }

    async getPaginated(
        pageOptionsDto: ProductionsPageOptionsDto,
    ): Promise<ProductionsPageDto> {
        const queryBuilder = this.productionRepository.createQueryBuilder(
            'production',
        );
        const [productions, productionsCount] = await queryBuilder
            .leftJoinAndSelect('production.links', 'link')
            .skip(pageOptionsDto.skip)
            .take(pageOptionsDto.take)
            .getManyAndCount();

        const pageMetaDto = new PageMetaDto({
            pageOptionsDto,
            itemCount: productionsCount,
        });

        return new ProductionsPageDto(productions.toDtos(), pageMetaDto);
    }

    async findOne(id: string): Promise<ProductionEntity> {
        const queryBuilder = this.productionRepository.createQueryBuilder(
            'production',
        );
        const production = await queryBuilder.where('production.id = :id', {
            id,
        });

        return production.getOne();
    }

    async updateProduction(
        id: string,
        updateProduction: UpdateProductionDto,
        file: IFile,
    ): Promise<ProductionEntity> {
        const production = await this.findOne(id);

        if (!production) {
            throw new ProductionNotFoundException();
        }

        if (file && !this.validatorService.isImage(file.mimetype)) {
            throw new FileNotImageException();
        }

        if (file) {
            production.logo = await this.awsS3Service.updateImage(
                file,
                production.logo,
            );
        }

        const updatedProduction = Object.assign(production, updateProduction);

        return this.productionRepository.save(updatedProduction);
    }

    async removeProduction(id: string): Promise<ProductionEntity> {
        const production = await this.findOne(id);

        if (!production) {
            throw new ProductionNotFoundException();
        }

        if (production.logo) {
            await this.awsS3Service.removeImage(production.logo);
        }

        return this.productionRepository.remove(production);
    }
}
