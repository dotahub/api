import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { ProductionEntity } from './production.entity';

@EntityRepository(ProductionEntity)
export class ProductionRepository extends Repository<ProductionEntity> {}
