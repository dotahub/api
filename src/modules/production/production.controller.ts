import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    ParseUUIDPipe,
    Patch,
    Post,
    Query,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
    ApiBearerAuth,
    ApiImplicitFile,
    ApiOkResponse,
    ApiOperation,
    ApiUseTags,
} from '@nestjs/swagger';

import { RoleType } from '../../common/constants/role-type';
import { Roles } from '../../decorators/roles.decorator';
import { ProductionNotFoundException } from '../../exceptions/production-not-found.exception';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { IFile } from '../../interfaces/IFile';
import { CreateProductionDto } from './dto/CreateProductionDto';
import { ProductionDto } from './dto/ProductionDto';
import { ProductionsPageDto } from './dto/ProductionsPageDto';
import { ProductionsPageOptionsDto } from './dto/ProductionsPageOptionsDto';
import { UpdateProductionDto } from './dto/UpdateProductionDto';
import { ProductionService } from './production.service';

@Controller('production')
@ApiUseTags('production')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
export class ProductionController {
    constructor(public readonly productionService: ProductionService) {}

    @Post('add')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Create production',
    })
    @ApiOkResponse({
        type: ProductionDto,
        description: 'Production data',
    })
    @ApiImplicitFile({ name: 'logo', required: true })
    @UseInterceptors(FileInterceptor('logo'))
    async addProduction(
        @Body() createProductionDto: CreateProductionDto,
        @UploadedFile() file: IFile,
    ): Promise<ProductionDto> {
        const addedProduction = await this.productionService.addProduction(
            createProductionDto,
            file,
        );

        return addedProduction.toDto();
    }

    @Get('')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Get productions paginated',
    })
    @ApiOkResponse({
        description: 'Return all productions',
        type: ProductionsPageDto,
    })
    paginated(
        @Query(new ValidationPipe({ transform: true }))
        pageOptionsDto: ProductionsPageOptionsDto,
    ): Promise<ProductionsPageDto> {
        return this.productionService.getPaginated(pageOptionsDto);
    }

    @Get(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Get production by UUID',
    })
    @ApiOkResponse({
        description: 'Return production by UUID',
        type: ProductionDto,
    })
    async findOne(
        @Param('id', new ParseUUIDPipe()) id,
    ): Promise<ProductionDto | any> {
        const production = await this.productionService.findOne(id);

        if (!production) {
            throw new ProductionNotFoundException();
        }

        return production.toDto();
    }

    @Patch(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Update production by UUID',
    })
    @ApiOkResponse({
        description: 'Production updated',
        type: ProductionDto,
    })
    @ApiImplicitFile({ name: 'logo', required: true })
    @UseInterceptors(FileInterceptor('logo'))
    async update(
        @Param('id', new ParseUUIDPipe()) id,
        @Body() updateProductionDto: UpdateProductionDto,
        @UploadedFile() file: IFile,
    ): Promise<ProductionDto> {
        const updatedProduction = await this.productionService.updateProduction(
            id,
            updateProductionDto,
            file,
        );

        return updatedProduction.toDto();
    }

    @Delete(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Delete production by UUID',
    })
    @ApiOkResponse({
        description: 'Delete production',
    })
    async remove(@Param('id', new ParseUUIDPipe()) id): Promise<ProductionDto> {
        const removeProduction = await this.productionService.removeProduction(
            id,
        );

        return removeProduction.toDto();
    }
}
