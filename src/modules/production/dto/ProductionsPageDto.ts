import { ApiModelProperty } from '@nestjs/swagger';

import { PageMetaDto } from '../../../common/dto/PageMetaDto';
import { ProductionDto } from './ProductionDto';

export class ProductionsPageDto {
    @ApiModelProperty({
        type: ProductionDto,
        isArray: true,
    })
    readonly data: ProductionDto[];

    @ApiModelProperty()
    readonly meta: PageMetaDto;

    constructor(data: ProductionDto[], meta: PageMetaDto) {
        this.data = data;
        this.meta = meta;
    }
}
