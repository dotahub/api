'use strict';

import { ApiModelProperty } from '@nestjs/swagger';
import * as slug from 'slug';

import { AbstractDto } from '../../../common/dto/AbstractDto';
import { LinkDto } from '../../../modules/link/dto/LinkDto';
import { ProductionEntity } from '../production.entity';

export class ProductionDto extends AbstractDto {
    @ApiModelProperty()
    readonly title: string;

    @ApiModelProperty()
    slug: string;

    @ApiModelProperty()
    logo: string;

    @ApiModelProperty()
    readonly about: string;

    @ApiModelProperty({ type: LinkDto, isArray: true })
    links: LinkDto[];

    constructor(production: ProductionEntity) {
        super(production);
        this.title = production.title;
        this.slug = slug(production.slug, { lower: true });
        this.logo = production.logo;
        this.about = production.about;
        this.links = production.links.toDtos();
    }
}
