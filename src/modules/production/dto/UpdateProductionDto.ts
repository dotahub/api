import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class UpdateProductionDto {
    @IsString()
    @IsOptional()
    @MinLength(4)
    @MaxLength(25)
    @ApiModelPropertyOptional()
    title: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    slug: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    about: string;
}
