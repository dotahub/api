'use strict';

import { ApiModelProperty } from '@nestjs/swagger';
import {
    IsArray,
    IsNotEmpty,
    IsOptional,
    IsString,
    MaxLength,
    MinLength,
} from 'class-validator';

import { CreateLinkDto } from '../../../modules/link/dto/CreateLinkDto';

export class CreateProductionDto {
    @IsString()
    @MinLength(4)
    @MaxLength(25)
    @ApiModelProperty()
    readonly title: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    slug: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    readonly about: string;

    @IsOptional()
    @IsArray()
    @ApiModelProperty({ type: CreateLinkDto, isArray: true })
    links: CreateLinkDto[];
}
