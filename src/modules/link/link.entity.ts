import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../../common/abstract.entity';
import { ProductionEntity } from '../../modules/production/production.entity';
import { LinkDto } from './dto/LinkDto';
import { LinkType } from './link-type';

@Entity({ name: 'link' })
export class LinkEntity extends AbstractEntity<LinkDto> {
    @Column('text')
    url: string;

    @Column({ type: 'enum', enum: LinkType })
    type: LinkType;

    @ManyToOne(
        () => ProductionEntity,
        (production: ProductionEntity) => production.links,
    )
    production: ProductionEntity;

    dtoClass = LinkDto;
}
