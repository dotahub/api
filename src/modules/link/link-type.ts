'use strict';

export enum LinkType {
    TWITTER = 'TWITTER',
    VK = 'VK',
    INSTAGRAM = 'INSTAGRAM',
    TELEGRAM = 'TELEGRAM',
    FACEBOOK = 'FACEBOOK',
    YOUTUBE = 'YOUTUBE',
    TWITCH = 'TWITCH',
    DISCORD = 'DISCORD',
    STEAM = 'STEAM',
    DOTABUFF = 'DOTABUFF',
}
