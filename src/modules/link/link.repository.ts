import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { LinkEntity } from './link.entity';

@EntityRepository(LinkEntity)
export class LinkRepository extends Repository<LinkEntity> {}
