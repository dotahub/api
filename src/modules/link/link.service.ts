import { Injectable } from '@nestjs/common';

import { PageMetaDto } from '../../common/dto/PageMetaDto';
import { LinkNotFoundException } from '../../exceptions/link-not-found-exception';
import { WrongLinkTypeException } from '../../exceptions/wrong-link-type.exception';
import { ValidatorService } from '../../shared/services/validator.service';
import { CreateLinkDto } from './dto/CreateLinkDto';
import { LinksPageDto } from './dto/LinksPageDto';
import { LinksPageOptionsDto } from './dto/LinksPageOptionsDto';
import { UpdateLinkDto } from './dto/UpdateLinkDto';
import { LinkEntity } from './link.entity';
import { LinkRepository } from './link.repository';

@Injectable()
export class LinkService {
    constructor(
        public readonly linkRepository: LinkRepository,
        public readonly validatorService: ValidatorService,
    ) {}

    async addLink(addLinkDto: CreateLinkDto): Promise<LinkEntity> {
        const { url, type } = addLinkDto;

        if (!this.validatorService.isLinkCorrectType(url, type)) {
            throw new WrongLinkTypeException();
        }

        const link = this.linkRepository.create(addLinkDto);

        return this.linkRepository.save(link);
    }

    async getPaginated(
        pageOptionsDto: LinksPageOptionsDto,
    ): Promise<LinksPageDto> {
        const queryBuilder = this.linkRepository.createQueryBuilder('link');

        const [links, linksCount] = await queryBuilder
            .skip(pageOptionsDto.skip)
            .take(pageOptionsDto.take)
            .getManyAndCount();

        const pageMetaDto = new PageMetaDto({
            pageOptionsDto,
            itemCount: linksCount,
        });

        return new LinksPageDto(links.toDtos(), pageMetaDto);
    }

    async findOne(id: string): Promise<LinkEntity> {
        const queryBuilder = this.linkRepository.createQueryBuilder('link');
        const link = await queryBuilder.where('link.id = :id', {
            id,
        });

        return link.getOne();
    }

    async updateLink(
        id: string,
        updateLink: UpdateLinkDto,
    ): Promise<LinkEntity> {
        const link = await this.findOne(id);

        if (!link) {
            throw new LinkNotFoundException();
        }

        const { url, type } = updateLink;
        if (!this.validatorService.isLinkCorrectType(url, type)) {
            throw new WrongLinkTypeException();
        }

        const updatedlink = Object.assign(link, updateLink);

        return this.linkRepository.save(updatedlink);
    }

    async removelink(id: string): Promise<LinkEntity> {
        const link = await this.findOne(id);

        if (!link) {
            throw new LinkNotFoundException();
        }

        return this.linkRepository.remove(link);
    }
}
