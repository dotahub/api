export const whitelistHosts = [
    'twitter.com',
    'vk.com',
    'facebook.com',
    'instagram.com',
    'twitch.tv',
    'youtube.com',
    'www.youtube.com',
    'discord.gg',
    'dotabuff.com',
    'steamcommunity.com',
    't.me',
];
