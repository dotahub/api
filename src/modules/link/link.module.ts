import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LinkController } from './link.controller';
import { LinkRepository } from './link.repository';
import { LinkService } from './link.service';

@Module({
    imports: [TypeOrmModule.forFeature([LinkRepository])],
    providers: [LinkService],
    controllers: [LinkController],
})
export class LinkModule {}
