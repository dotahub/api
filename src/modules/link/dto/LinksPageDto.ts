import { ApiModelProperty } from '@nestjs/swagger';

import { PageMetaDto } from '../../../common/dto/PageMetaDto';
import { LinkDto } from './LinkDto';

export class LinksPageDto {
    @ApiModelProperty({
        type: LinkDto,
        isArray: true,
    })
    readonly data: LinkDto[];

    @ApiModelProperty()
    readonly meta: PageMetaDto;

    constructor(data: LinkDto[], meta: PageMetaDto) {
        this.data = data;
        this.meta = meta;
    }
}
