'use strict';

import { ApiModelProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsUrl, IsUUID } from 'class-validator';

import { LinkType } from '../link-type';
import { whitelistHosts } from '../whitelist-hosts';

export class CreateLinkDto {
    @IsUrl({
        // eslint-disable-next-line camelcase
        require_host: true,
        // eslint-disable-next-line camelcase
        host_whitelist: whitelistHosts,
    })
    @IsNotEmpty()
    @ApiModelProperty({
        required: true,
    })
    url: string;

    @IsNotEmpty()
    @IsEnum(LinkType)
    @ApiModelProperty({
        description: 'Type of link',
        enum: LinkType,
        required: true,
    })
    type: LinkType;

    @IsNotEmpty()
    @IsUUID()
    productionUuid: string;
}
