'use strict';

import { ApiModelProperty } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/AbstractDto';
import { ProductionDto } from '../../../modules/production/dto/ProductionDto';
import { LinkType } from '../link-type';
import { LinkEntity } from '../link.entity';

export class LinkDto extends AbstractDto {
    @ApiModelProperty()
    url: string;

    @ApiModelProperty({ enum: LinkType })
    type: LinkType;

    @ApiModelProperty({ type: ProductionDto })
    production: ProductionDto;

    constructor(link: LinkEntity) {
        super(link);
        this.url = link.url;
        this.type = link.type;
        this.production = link.production.toDto();
    }
}
