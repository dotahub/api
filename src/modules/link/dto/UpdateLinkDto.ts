'use strict';

import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsUrl } from 'class-validator';

import { LinkType } from '../link-type';
import { whitelistHosts } from '../whitelist-hosts';

export class UpdateLinkDto {
    @IsUrl({
        // eslint-disable-next-line camelcase
        require_host: true,
        // eslint-disable-next-line camelcase
        host_whitelist: whitelistHosts,
    })
    @IsOptional()
    @ApiModelPropertyOptional()
    url: string;

    @IsOptional()
    @IsEnum(LinkType)
    @ApiModelPropertyOptional({
        description: 'Type of link',
        enum: LinkType,
    })
    type: LinkType;
}
