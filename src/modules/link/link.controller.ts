import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    ParseUUIDPipe,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
    ValidationPipe,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiOkResponse,
    ApiOperation,
    ApiUseTags,
} from '@nestjs/swagger';

import { RoleType } from '../../common/constants/role-type';
import { Roles } from '../../decorators/roles.decorator';
import { LinkNotFoundException } from '../../exceptions/link-not-found-exception';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { CreateLinkDto } from './dto/CreateLinkDto';
import { LinkDto } from './dto/LinkDto';
import { LinksPageDto } from './dto/LinksPageDto';
import { LinksPageOptionsDto } from './dto/LinksPageOptionsDto';
import { UpdateLinkDto } from './dto/UpdateLinkDto';
import { LinkService } from './link.service';

@Controller('link')
@ApiUseTags('link')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
export class LinkController {
    constructor(public readonly linkService: LinkService) {}

    @Post('add')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Create production social link',
    })
    @ApiOkResponse({
        type: LinkDto,
        description: 'Link data',
    })
    async addLink(@Body() createLinkDto: CreateLinkDto): Promise<LinkDto> {
        const addedLink = await this.linkService.addLink(createLinkDto);

        return addedLink.toDto();
    }

    @Get()
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Get paginated links',
    })
    @ApiOkResponse({
        description: 'Return all paginated links',
        type: LinksPageDto,
    })
    paginated(
        @Query(new ValidationPipe({ transform: true }))
        pageOptionsDto: LinksPageOptionsDto,
    ): Promise<LinksPageDto> {
        return this.linkService.getPaginated(pageOptionsDto);
    }

    @Get(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Get social link by UUID',
    })
    @ApiOkResponse({
        description: 'Return production social link by UUID',
        type: LinkDto,
    })
    async findOne(
        @Param('id', new ParseUUIDPipe()) id,
    ): Promise<LinkDto | any> {
        const link = await this.linkService.findOne(id);

        if (!link) {
            throw new LinkNotFoundException();
        }

        return link.toDto();
    }

    @Patch(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Update link by UUID',
    })
    @ApiOkResponse({
        description: 'Link updated',
        type: LinkDto,
    })
    async update(
        @Param('id', new ParseUUIDPipe()) id,
        @Body() updateLinkDto: UpdateLinkDto,
    ): Promise<LinkDto> {
        const updatedLink = await this.linkService.updateLink(
            id,
            updateLinkDto,
        );

        return updatedLink.toDto();
    }

    @Delete(':id')
    @Roles(RoleType.ADMIN)
    @HttpCode(HttpStatus.OK)
    @ApiOperation({
        title: 'Delete link by UUID',
    })
    @ApiOkResponse({
        description: 'Delete link',
    })
    async remove(@Param('id', new ParseUUIDPipe()) id): Promise<LinkDto> {
        const removeLink = await this.linkService.removelink(id);

        return removeLink.toDto();
    }
}
