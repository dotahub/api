import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';

import { LinkType } from '../../modules/link/link-type';

@Injectable()
export class ValidatorService {
    public isImage(mimeType: string): boolean {
        const imageMimeTypes = ['image/jpeg', 'image/png'];

        return _.includes(imageMimeTypes, mimeType);
    }

    public isLinkCorrectType(url: string, type: LinkType): boolean {
        const lowerType = LinkType[type].toLowerCase();
        return new RegExp(lowerType).test(url);
    }
}
