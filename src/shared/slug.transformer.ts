import * as slug from 'slug';
import { ValueTransformer } from 'typeorm';

/**
 * Convert string to slug
 * Example: "Some Text" => "some-text"
 */
export class SlugTransformer implements ValueTransformer {
    to(value) {
        return slug(value, { lower: true });
    }
    from(value) {
        return value;
    }
}
