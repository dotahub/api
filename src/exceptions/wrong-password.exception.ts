'use strict';

import { NotFoundException } from '@nestjs/common';

export class WrongPasswordException extends NotFoundException {
    constructor(error?: string) {
        super('error.wrong_password', error);
    }
}
