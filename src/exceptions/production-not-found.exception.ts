'use strict';

import { NotFoundException } from '@nestjs/common';

export class ProductionNotFoundException extends NotFoundException {
    constructor(error?: string) {
        super('error.production_not_found', error);
    }
}
