'use strict';

import { NotFoundException } from '@nestjs/common';

export class LinkNotFoundException extends NotFoundException {
    constructor(error?: string) {
        super('error.link_not_found', error);
    }
}
