import { BadRequestException } from '@nestjs/common';

export class WrongLinkTypeException extends BadRequestException {
    constructor(message?: string | object | any, error?: string) {
        if (message) {
            super(message, error);
        } else {
            super('error.link.wrong_type');
        }
    }
}
